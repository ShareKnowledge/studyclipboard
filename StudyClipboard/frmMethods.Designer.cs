﻿namespace StudyClipboard
{
    partial class frmMethods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClear = new System.Windows.Forms.Button();
            this.btnContains = new System.Windows.Forms.Button();
            this.btnContainsImage = new System.Windows.Forms.Button();
            this.btnGetPath = new System.Windows.Forms.Button();
            this.btnSetData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(25, 12);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(146, 23);
            this.btnClear.TabIndex = 0;
            this.btnClear.Text = "清除剪切板上的数据";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnContains
            // 
            this.btnContains.Location = new System.Drawing.Point(206, 12);
            this.btnContains.Name = "btnContains";
            this.btnContains.Size = new System.Drawing.Size(179, 23);
            this.btnContains.TabIndex = 1;
            this.btnContains.Text = "判断是否包含指定的数据";
            this.btnContains.UseVisualStyleBackColor = true;
            this.btnContains.Click += new System.EventHandler(this.btnContains_Click);
            // 
            // btnContainsImage
            // 
            this.btnContainsImage.Location = new System.Drawing.Point(420, 12);
            this.btnContainsImage.Name = "btnContainsImage";
            this.btnContainsImage.Size = new System.Drawing.Size(179, 23);
            this.btnContainsImage.TabIndex = 2;
            this.btnContainsImage.Text = "判断是否包含图片";
            this.btnContainsImage.UseVisualStyleBackColor = true;
            this.btnContainsImage.Click += new System.EventHandler(this.btnContainsImage_Click);
            // 
            // btnGetPath
            // 
            this.btnGetPath.Location = new System.Drawing.Point(25, 76);
            this.btnGetPath.Name = "btnGetPath";
            this.btnGetPath.Size = new System.Drawing.Size(146, 23);
            this.btnGetPath.TabIndex = 3;
            this.btnGetPath.Text = "获取剪切板上的路径";
            this.btnGetPath.UseVisualStyleBackColor = true;
            this.btnGetPath.Click += new System.EventHandler(this.btnGetPath_Click);
            // 
            // btnSetData
            // 
            this.btnSetData.Location = new System.Drawing.Point(206, 76);
            this.btnSetData.Name = "btnSetData";
            this.btnSetData.Size = new System.Drawing.Size(179, 23);
            this.btnSetData.TabIndex = 4;
            this.btnSetData.Text = "向剪切板中放置数据";
            this.btnSetData.UseVisualStyleBackColor = true;
            this.btnSetData.Click += new System.EventHandler(this.btnSetData_Click);
            // 
            // frmMethods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 246);
            this.Controls.Add(this.btnSetData);
            this.Controls.Add(this.btnGetPath);
            this.Controls.Add(this.btnContainsImage);
            this.Controls.Add(this.btnContains);
            this.Controls.Add(this.btnClear);
            this.Name = "frmMethods";
            this.Text = "frmMethods";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnContains;
        private System.Windows.Forms.Button btnContainsImage;
        private System.Windows.Forms.Button btnGetPath;
        private System.Windows.Forms.Button btnSetData;
    }
}