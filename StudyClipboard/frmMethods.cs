﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudyClipboard
{
    public partial class frmMethods : Form
    {
        public frmMethods()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //清除剪切板上的数据
            Clipboard.Clear();
        }

        /// <summary>
        /// 判断剪贴板中是否包含文本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnContains_Click(object sender, EventArgs e)
        {
            bool flag = Clipboard.ContainsText();
            Console.WriteLine(flag);
        }

        /// <summary>
        /// 判断剪切板中是否包含图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnContainsImage_Click(object sender, EventArgs e)
        {
            //将图片放置到剪切板中
            Image img = Image.FromFile("file.png");
            Clipboard.SetImage(img);

            bool flag = Clipboard.ContainsImage();
            Console.WriteLine(flag);
        }

        /// <summary>
        /// 获取剪切板上的路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetPath_Click(object sender, EventArgs e)
        {
            StringCollection sc = Clipboard.GetFileDropList();
            foreach (var item in sc)
            {
                Console.WriteLine(item.ToString());
            }
        }

        /// <summary>
        /// 向剪切板中放置数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetData_Click(object sender, EventArgs e)
        {
            string data = "天王盖地虎!";
            Clipboard.SetDataObject(data);

            string text = Clipboard.GetText();
            Console.WriteLine(text);
        }
    }
}
